#pragma once
#include <iostream>
#include <srrg_types/defs.h>
#include <srrg_boss/blob.h>

namespace srrg_core_map {
struct TraversabilityMap: public srrg_boss::BLOB{
    TraversabilityMap(const srrg_core::UnsignedCharImage& img = srrg_core::UnsignedCharImage::zeros(0,0),
                      const srrg_core::IntImage& indices = srrg_core::IntImage::zeros(0,0),
                      srrg_core::FloatImage elevations = srrg_core::FloatImage::zeros(0,0)) {
        img.copyTo(_img);
        indices.copyTo(_indices);
        elevations.copyTo(_elevations);
    }
    //! saves the cloud in a binary stream, optimized for speed
    virtual void write(std::ostream& os) const;
    //! loads the cloud from a binary stream, optimized for speed
    virtual bool read(std::istream& is);
    inline const srrg_core::UnsignedCharImage& image() const { return _img;}
    inline const srrg_core::IntImage& indices() const { return _indices;}
    inline const srrg_core::FloatImage& elevations() const {return _elevations;}
    srrg_core::UnsignedCharImage _img;
    srrg_core::IntImage _indices;
    srrg_core::FloatImage _elevations;
};
typedef srrg_boss::BLOBReference<TraversabilityMap> TraversabilityMapBLOBReference;
}
